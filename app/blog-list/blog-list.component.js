'use strict';

angular.module('blogList').
        component('blogList', {
           // template: "<div class=''> <h1 class='new-class'>{{ title }}</h1> <button ng-click='someClickTest()'>Click me!</button> </div>",
         templateUrl:"templates/blog-list.html",
            controller: function($routeParams, $scope){
               console.log($routeParams)
               var blogItems = [
                {title: "Heading Title", id: 1, description: "This is book", publishDate: "2025-2026"},
                {title: "Title", id: 2, description: "This is book"},
                {title: "Tea", id: 3, description: "This is book"},
                {title: "Lite", id: 4, description: "This is book"},
               ]

               $scope.items = blogItems;

                $scope.title = 'Hi there'
            $scope.clicks = 0
            $scope.someClickTest = function(){
            console.log("clicked")
            $scope.clicks += 1
            $scope.title = 'Clicked ' + $scope.clicks + ' times'

         }
        }
       
        })
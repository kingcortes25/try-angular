'use strict';

angular.module('try', [
    
    'ngResource',
    'ngRoute',
  
    'blogList',
    'blogDetail'
]);